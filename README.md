# The BAJer importer

Importer to fetch data from the [ADS](https://ui.adsabs.harvard.edu).
To be used with [the BAJer backend](https://gitlab.com/thebajer/bajer-backend).

## Getting started

Change into the repo directory:
``` bash
cd bajer-importer
```

Install the requirements
``` bash
pip install -r requirements.txt
```

Setup the database (as the `postgres` user):
Create user and database:
``` bash
createuser bajer
createdb bajer --lc-collate="en_US.UTF-8" --lc-ctype="en_US.UTF-8" -O bajer -E UTF8 -T template0
```
Grant permissions (as the `postgres` user)::
``` bash
psql
GRANT ALL PRIVILEGES ON DATABASE bajer TO bajer;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA bajer TO bajer;
```

Change password of the `bajer` user (as `postgres` user):
``` bash
psql
\password bajer
```

Create tables (as:
``` bash
psql -d bajer -f bajer.sql
```
Create the config file at `~/.bajer/bajer-importer.yaml` where `~` is the home directory of the user running the importer. Adapt the search query to whatever you need. Test if the results are what you expect by entering it manually in the [ADS](https://ui.adsabs.harvard.edu/).
``` yaml
database: postgres://<connection string>
search: '(bibstem:"A&A" OR bibstem:"APJ" OR bibstem:"AJ" OR bibstem:"MNRAS")'
```

Run the importer:
``` bash
python -m bajer-importer
```

