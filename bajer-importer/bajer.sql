--
-- PostgreSQL database dump
--

-- Dumped from database version 11.2
-- Dumped by pg_dump version 11.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: authors; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.authors (
    id bigint NOT NULL,
    author text
);


--
-- Name: authors_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.authors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: authors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.authors_id_seq OWNED BY public.authors.id;


--
-- Name: authors_papers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.authors_papers (
    id_authors bigint NOT NULL,
    bibcode_papers character varying(19) NOT NULL
);


--
-- Name: citing_papers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.citing_papers (
    bibcode character varying(19) NOT NULL,
    year smallint NOT NULL,
    first_fetched date NOT NULL,
    ref_bibcode character varying(19) NOT NULL,
    ref_year smallint NOT NULL,
    ref_journal character varying(5) NOT NULL
);


--
-- Name: countries; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.countries (
    id bigint NOT NULL,
    country text
);


--
-- Name: countries_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.countries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: countries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.countries_id_seq OWNED BY public.countries.id;


--
-- Name: countries_papers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.countries_papers (
    id_countries bigint NOT NULL,
    bibcode_papers character varying(19) NOT NULL,
    count smallint NOT NULL
);


--
-- Name: fetch_dates; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.fetch_dates (
    date date NOT NULL,
    released boolean NOT NULL
);


--
-- Name: histories; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.histories (
    citation_count integer NOT NULL,
    paper character varying(19) NOT NULL,
    date date NOT NULL
);


--
-- Name: journals; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.journals (
    bibstem character varying(5) NOT NULL,
    name text
);


--
-- Name: keywords; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.keywords (
    id bigint NOT NULL,
    keyword text
);


--
-- Name: keywords_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.keywords_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: keywords_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.keywords_id_seq OWNED BY public.keywords.id;


--
-- Name: keywords_papers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.keywords_papers (
    id_keywords bigint NOT NULL,
    bibcode_papers character varying(19) NOT NULL
);


--
-- Name: papers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.papers (
    bibcode character varying(19) NOT NULL,
    title text,
    abstract text,
    first_author bigint,
    first_author_country bigint,
    pubdate date NOT NULL,
    journal character varying(5)
);


--
-- Name: authors id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.authors ALTER COLUMN id SET DEFAULT nextval('public.authors_id_seq'::regclass);


--
-- Name: countries id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.countries ALTER COLUMN id SET DEFAULT nextval('public.countries_id_seq'::regclass);


--
-- Name: keywords id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.keywords ALTER COLUMN id SET DEFAULT nextval('public.keywords_id_seq'::regclass);


--
-- Name: authors author_uq; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.authors
    ADD CONSTRAINT author_uq UNIQUE (author);


--
-- Name: authors_papers authors_papers_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.authors_papers
    ADD CONSTRAINT authors_papers_pk PRIMARY KEY (id_authors, bibcode_papers);


--
-- Name: authors authors_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.authors
    ADD CONSTRAINT authors_pk PRIMARY KEY (id);


--
-- Name: citing_papers citing_papers_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.citing_papers
    ADD CONSTRAINT citing_papers_pk PRIMARY KEY (bibcode, ref_bibcode);


--
-- Name: countries_papers countries_papers_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.countries_papers
    ADD CONSTRAINT countries_papers_pk PRIMARY KEY (id_countries, bibcode_papers);


--
-- Name: countries countries_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.countries
    ADD CONSTRAINT countries_pk PRIMARY KEY (id);


--
-- Name: countries country_uq; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.countries
    ADD CONSTRAINT country_uq UNIQUE (country);


--
-- Name: fetch_dates fetch_dates_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.fetch_dates
    ADD CONSTRAINT fetch_dates_pk PRIMARY KEY (date);


--
-- Name: histories histories_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.histories
    ADD CONSTRAINT histories_pk PRIMARY KEY (paper, date);


--
-- Name: journals journals_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.journals
    ADD CONSTRAINT journals_pk PRIMARY KEY (bibstem);


--
-- Name: keywords keyword_uq; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.keywords
    ADD CONSTRAINT keyword_uq UNIQUE (keyword);


--
-- Name: keywords_papers keywords_papers_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.keywords_papers
    ADD CONSTRAINT keywords_papers_pk PRIMARY KEY (id_keywords, bibcode_papers);


--
-- Name: keywords keywords_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.keywords
    ADD CONSTRAINT keywords_pk PRIMARY KEY (id);


--
-- Name: papers papers_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.papers
    ADD CONSTRAINT papers_pk PRIMARY KEY (bibcode);


--
-- Name: author_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX author_idx ON public.authors USING btree (author);


--
-- Name: country_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX country_idx ON public.countries USING btree (country);


--
-- Name: keyword_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX keyword_idx ON public.keywords USING btree (keyword);


--
-- Name: authors_papers authors_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.authors_papers
    ADD CONSTRAINT authors_fk FOREIGN KEY (id_authors) REFERENCES public.authors(id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: countries_papers countries_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.countries_papers
    ADD CONSTRAINT countries_fk FOREIGN KEY (id_countries) REFERENCES public.countries(id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: histories fetch_dates_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.histories
    ADD CONSTRAINT fetch_dates_fk FOREIGN KEY (date) REFERENCES public.fetch_dates(date) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: papers first_author_country_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.papers
    ADD CONSTRAINT first_author_country_fk FOREIGN KEY (first_author_country) REFERENCES public.countries(id) MATCH FULL;


--
-- Name: papers first_author_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.papers
    ADD CONSTRAINT first_author_fk FOREIGN KEY (first_author) REFERENCES public.authors(id) MATCH FULL;


--
-- Name: papers journal_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.papers
    ADD CONSTRAINT journal_fk FOREIGN KEY (journal) REFERENCES public.journals(bibstem) MATCH FULL ON UPDATE CASCADE ON DELETE SET NULL;


--
-- Name: keywords_papers keywords_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.keywords_papers
    ADD CONSTRAINT keywords_fk FOREIGN KEY (id_keywords) REFERENCES public.keywords(id) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: histories papers_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.histories
    ADD CONSTRAINT papers_fk FOREIGN KEY (paper) REFERENCES public.papers(bibcode) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: keywords_papers papers_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.keywords_papers
    ADD CONSTRAINT papers_fk FOREIGN KEY (bibcode_papers) REFERENCES public.papers(bibcode) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: authors_papers papers_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.authors_papers
    ADD CONSTRAINT papers_fk FOREIGN KEY (bibcode_papers) REFERENCES public.papers(bibcode) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: citing_papers papers_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.citing_papers
    ADD CONSTRAINT papers_fk FOREIGN KEY (ref_bibcode) REFERENCES public.papers(bibcode) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: countries_papers papers_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.countries_papers
    ADD CONSTRAINT papers_fk FOREIGN KEY (bibcode_papers) REFERENCES public.papers(bibcode) MATCH FULL ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- PostgreSQL database dump complete
--

