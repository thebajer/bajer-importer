def dofilter(row):
    """Filter for empty first_author"""
    if row.first_author is not None and row.first_author.strip():
        return (True, None)
    else:
        return (False, "First author empty.")
