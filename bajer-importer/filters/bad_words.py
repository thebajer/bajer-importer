def get_bad_words():
    """Return list of words to filter out."""
    return [
        "errata",
        "erratum",
        "addenda",
        "addendum",
        "letter",
        "supplement",
        "corrigendum",
        "announcement",
        "editorial",
        "obituary",
        "notice",
    ]


def dofilter(row):
    """Filter for certain bad words in title and keywords
    """

    bad_words = get_bad_words()
    if not row.title or not row.title[0]:
        return (False, "Empty title.")
    for bw in bad_words:
        if bw and bw in row.title[0].lower():
            return (False, f"Found bad word --{bw}-- in title.")

    # keywords = (str.lower(w.strip()) for w in row.keyword.split(';'))
    if row.keyword:
        for kw in row.keyword:
            for bw in bad_words:
                if bw in kw.lower():
                    return (False, f"Found bad word --{bw}-- in keywords.")

    return (True, None)
