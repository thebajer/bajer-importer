import argparse
import ads

# import ads.sandbox as ads
import logging
import psycopg2
import sys
import datetime
from . import runtests, store
import yaml
from pathlib import Path

parser = argparse.ArgumentParser()
parser.add_argument(
    "-d",
    "--database",
    type=str,
    help="Postgres connection string to database. If not set the default config file at ~/.bajer/bajer-importer.yaml is used.",
)
parser.add_argument(
    "-p", "--logpath", default="bajer_importer.log", help="Path to log file."
)
parser.add_argument("-l", "--level", default="error", help="Log level.")

args = parser.parse_args()

home = str(Path.home())
config = yaml.safe_load(open(home + "/.bajer/bajer-importer.yaml"))

if args.database is None:
    db_conn_str = config["database"]
else:
    db_conn_str = args.database

# convert log level from cli argument to numeric value
numeric_log_level = getattr(logging, args.level.upper(), None)
if not isinstance(numeric_log_level, int):
    raise ValueError("Invalid log level: %s" % args.level)

fetch_date = datetime.datetime.utcnow()

logging.basicConfig(
    format="%(asctime)s - %(levelname)s - %(name)s - %(message)s",
    filename=args.logpath,
    level=numeric_log_level,
)

log = logging.getLogger(__name__)


log.info("Application started")

# TODO make the following configureable?

fields = [
    "title",
    "pubdate",
    "abstract",
    "author",
    "bibcode",
    "citation_count",
    "citation",
    "date",
    "first_author",
    "indexstamp",
    "keyword",
    "year",
    "doi",
    "aff",
    "pub",
]

query = ""

if "search" in config.keys() and config["search"]:
    query = config["search"]
else:
    logging.error("Search term not defined in config file!")
    sys.stderr.write("Search term not defined in config file!")
    sys.exit(1)

store = store.store(db_conn_str, fetch_date)

# TODO make rows and pages configureable
# query results in ~250k hits.
ads_query = []
try:
    ads_query = ads.SearchQuery(q=query, fl=fields, max_pages=1000, rows=500)
except Exception as e:
    logging.error("Failed to fetch data from ads.")
    logging.error("%s", e)
    sys.stderr.write("Failed to fetch data from ads: {}".format(e))
    sys.exit(1)


passed = runtests.run_filter(ads_query)
counter = 0

log.info("Starting inserting to database.")
for item in passed:
    log.debug("Inserting %s to database", item.bibcode)
    store.store(item)
    counter += 1
    if counter % 1000 == 0:
        log.info("Imported %s papers.", counter)

store.finish()
log.info("Import finished. %s papers imported.", counter)
log.info("API Limits info: %s", ads_query.response.get_ratelimits())
