import logging
from .filters import *

log = logging.getLogger(__name__)

tests = [bad_words, author]


def run_filter(ads_query):
    """Run tests and yield the row if it passed them all."""
    for row in ads_query:
        log.debug("Apply filters to %s", row.bibcode)

        for test in tests:
            ret, logmsg = test.dofilter(row)
            if not ret:
                log.info("Filtered %s because of: %s", row.bibcode, logmsg)
                break
        else:
            log.debug("Filters passed: %s", row.bibcode)
            yield (row)
