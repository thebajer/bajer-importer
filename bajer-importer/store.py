import logging
import psycopg2
import psycopg2.extras
import sys
import html
import unicodedata
import Levenshtein as lv
import re
from . import countries

log = logging.getLogger(__name__)


class store:

    _conn = None
    _db = None

    _fetchdate = None

    _current_countries = []
    _countrylist = []

    _us_states = countries.us_states()

    def __init__(self, connstring, fetch_date):
        self._fetchdate = fetch_date
        try:
            self._conn = psycopg2.connect(connstring)
            self._db = self._conn.cursor()
        except Exception as e:
            log.error("Connecting to database failed.")
            log.error("%s", e)
            sys.stderr.write("Connecting to database failed: {}".format(e))
            sys.exit(1)

        self._store_fetchdate()

        self._countrylist = countries.countries()

    def store(self, item):
        """Insert. publication into database."""
        self._current_countries = []
        self._get_countries(item)

        self._store_keyword(item)
        self._store_authors(item)
        self._store_journal(item)
        self._store_countries(item)
        self._store_paper(item)
        self._store_histories(item, self._fetchdate)
        self._store_citing_papers(item, self._fetchdate)
        self._store_authors_papers(item)
        self._store_keywords_papers(item)
        self._store_countries_papers(item)

    def finish(self):
        """Finish import and commit transaction."""
        self._conn.commit()

    def _store_fetchdate(self):
        log.debug("Store fetchdate.")
        try:
            self._db.execute(
                """
                            INSERT INTO
                                fetch_dates
                            (
                                date,
                                released
                            )
                            VALUES
                            (
                                %s,
                                true
                            )
                            """,
                [self._fetchdate],
            )
        except Exception as e:
            log.error("Error inserting fetch date: %s", e)
            exit(1)

    def _store_keyword(self, item):
        if not item.keyword:
            log.debug("No keywords.")
            return
        log.debug("Storing keywords.")
        try:
            for kw in item.keyword:
                self._db.execute(
                    """
                                INSERT INTO
                                    keywords
                                (keyword)
                                VALUES
                                (%s)
                                ON CONFLICT DO NOTHING
                                """,
                    [kw],
                )
        except Exception as e:
            log.error("Error inserting keywords for %s: %s", item.bibcode, e)
            sys.exit(1)

    def _store_authors(self, item):
        log.debug("Storing authors.")
        try:
            for author in item.author:
                self._db.execute(
                    """
                                INSERT INTO
                                    authors
                                (author)
                                VALUES
                                (%s)
                                ON CONFLICT DO NOTHING
                                """,
                    [author],
                )
        except Exception as e:
            log.error("Error inserting authors for %s: %s", item.bibcode, e)
            sys.exit(1)

    def _store_journal(self, item):
        log.debug("Storing journal.")
        try:
            self._db.execute(
                """
                            INSERT INTO
                                journals
                            (
                                bibstem,
                                name
                            )
                            VALUES
                            (
                                %s,
                                %s
                            )
                            ON CONFLICT DO NOTHING
                            """,
                [self._bibcode_to_bibstem(item.bibcode), item.pub],
            )
        except Exception as e:
            log.error("Error inserting journal for %s: %s", item.bibcode, e)
            sys.exit(1)

    def _store_countries(self, item):
        log.debug("Storing countries.")
        if self._current_countries:
            try:
                countries = set(self._current_countries)
                for country in countries:
                    self._db.execute(
                        """
                                    INSERT INTO
                                        countries
                                    (country)
                                    VALUES
                                    (%s)
                                    ON CONFLICT DO NOTHING
                                    """,
                        [country],
                    )
            except Exception as e:
                log.error(
                    "Error inserting country for %s: %s", item.bibcode, e
                )
                sys.exit(1)

    def _store_paper(self, item):
        log.debug("Storing paper.")
        try:
            self._db.execute(
                """
                            INSERT INTO papers
                            (
                                bibcode,
                                title,
                                abstract,
                                first_author,
                                first_author_country,
                                pubdate,
                                journal
                            )
                            VALUES
                            (
                                %s,
                                %s,
                                %s,
                                (
                                    SELECT
                                        id
                                    FROM
                                        authors
                                    WHERE
                                        author = %s
                                ),
                                (
                                    SELECT
                                        id
                                    FROM
                                        countries
                                    WHERE
                                        country = %s
                                ),
                                %s,
                                %s
                            )
                            ON CONFLICT DO NOTHING
                            """,
                [
                    item.bibcode,
                    item.title[0],
                    item.abstract,
                    item.first_author,
                    self._current_countries[0]
                    if self._current_countries
                    else None,
                    self._fix_pub_date(item.pubdate),
                    self._bibcode_to_bibstem(item.bibcode),
                ],
            )
        except Exception as e:
            log.error("Error inserting paper for %s: %s", item.bibcode, e)
            sys.exit(1)

    def _store_histories(self, item, fetch_date):
        log.debug("Storing history.")
        try:
            self._db.execute(
                """
                            INSERT INTO
                                histories
                            (
                                citation_count,
                                paper,
                                date
                            )
                            VALUES
                            (
                                %s,
                                %s,
                                %s
                            )
                            ON CONFLICT DO NOTHING
                            """,
                [
                    item.citation_count if item.citation_count else 0,
                    item.bibcode,
                    self._fetchdate,
                ],
            )
        except Exception as e:
            log.error("Error inserting history for %s: %s", item.bibcode, e)
            sys.exit(1)

    def _store_citing_papers(self, item, fetch_date):
        if not item.citation:
            log.debug("No citing papers.")
            return
        log.debug("Storing citing papers.")
        try:
            for citing_paper in item.citation:
                log.debug("Storing citing paper: %s", citing_paper)
                self._db.execute(
                    """
                                INSERT INTO
                                    citing_papers
                                (
                                    bibcode,
                                    ref_bibcode,
                                    year,
                                    first_fetched,
                                    ref_year,
                                    ref_journal
                                )
                                VALUES
                                (
                                    %s,
                                    %s,
                                    %s,
                                    %s,
                                    %s,
                                    %s
                                )
                                ON CONFLICT DO NOTHING
                                """,
                    [
                        citing_paper,
                        item.bibcode,
                        self._bibcode_to_year(citing_paper),
                        fetch_date,
                        self._bibcode_to_year(item.bibcode),
                        self._bibcode_to_bibstem(item.bibcode),
                    ],
                )
        except Exception as e:
            log.error(
                "Error inserting citing papers for %s: %s", item.bibcode, e
            )
            sys.exit(1)

    def _store_authors_papers(self, item):
        log.debug("Storing authors paper relations.")
        try:
            for author in item.author:
                self._db.execute(
                    """
                                INSERT INTO
                                    authors_papers
                                (
                                    id_authors,
                                    bibcode_papers
                                )
                                VALUES
                                (
                                    (SELECT id FROM authors WHERE author = %s),
                                    %s
                                )
                                ON CONFLICT DO NOTHING
                                """,
                    [author, item.bibcode],
                )
        except Exception as e:
            log.error(
                "Error inserting authors paper relations for %s: %s",
                item.bibcode,
                e,
            )
            sys.exit(1)

    def _store_keywords_papers(self, item):
        if not item.keyword:
            log.debug("No keywords papers relations.")
            return
        log.debug("Storing keywords paper relations.")
        try:
            for kw in item.keyword:
                self._db.execute(
                    """
                                INSERT INTO
                                    keywords_papers
                                (
                                    id_keywords,
                                    bibcode_papers
                                )
                                VALUES
                                (
                                    (SELECT id FROM keywords WHERE keyword = %s),
                                    %s
                                )
                                ON CONFLICT DO NOTHING
                                """,
                    [kw, item.bibcode],
                )
        except Exception as e:
            log.error(
                "Error inserting keywords paper relations for %s: %s",
                item.bibcode,
                e,
            )
            sys.exit(1)

    def _store_countries_papers(self, item):
        # Check if there are any countries to store and abort early if not
        if not self._current_countries:
            log.info("No countries to store for paper %s", item.bibcode)
            return

        log.debug("Storing countries paper relations.")
        try:
            for country in set(self._current_countries):
                log.debug("Country for relation: %s", country)
                self._db.execute(
                    """
                                INSERT INTO
                                    countries_papers
                                (
                                    id_countries,
                                    bibcode_papers,
                                    count
                                )
                                VALUES
                                (
                                    (SELECT id FROM countries WHERE country = %s),
                                    %s,
                                    %s
                                )
                                ON CONFLICT DO NOTHING
                                """,
                    [
                        country[-255:],
                        item.bibcode,
                        self._current_countries.count(country),
                    ],
                )
        except Exception as e:
            log.error(
                "Error inserting countries paper relations for %s: %s",
                item.bibcode,
                e,
            )
            sys.exit(1)

    def _bibcode_to_bibstem(self, bibcode):
        """Extract bibstem from bibcode

        see https://en.wikipedia.org/wiki/Bibcode

        Format is YYYYJJJJJVVVVMPPPPA with `.` to fill not used characters
        """
        return bibcode[4:9].strip(".")

    def _bibcode_to_year(self, bibcode):
        """Extract year from bibcode

        see https://en.wikipedia.org/wiki/Bibcode

        Format is YYYYJJJJJVVVVMPPPPA with `.` to fill not used characters
        """
        return bibcode[:4]

    def _get_countries(self, item):
        """Extract the countries from the affiliation.

        They are stored in the aff field, a list of strings with the country
        being the last item separated by commas.

        Certain steps are taken to handle dirty data.
        TODO: Update description
        * HTML decode
        * Split by semicolon as multiple affiliations can be stored in one
            string separated by `;`
        """

        """
        TODO: Idea:
            Preps: html decode, strip of accents

            First try `in` with full name. If that does not work split
            try fuzz with a ratio >= 89?

            if that does not work, try matching US states -> USA?

            Always store 3char value

def strip_accents(s):
   return ''.join(c for c in unicodedata.normalize('NFD', s)
          if unicodedata.category(c) != 'Mn')

        """

        # log.debug('Aff: %s', item.aff)

        if item.aff:
            for key, line in enumerate(item.aff):
                res = self._extract_country(line)
                if res:
                    self._current_countries.append(res)
                else:
                    log.debug(
                        "No country found in line %s for %s", key, item.bibcode
                    )

    def _extract_country(self, data):
        cleaned = self._strip_accents(html.unescape(data)).lower()
        splitted = [item.strip(",; ") for item in cleaned.split()]
        log.debug(splitted)
        # Check if Country in string with word bound regex
        for country in self._countrylist:
            if any(
                re.search(f"\\b{variant}\\b", cleaned) for variant in country
            ):
                return country[0]

        log.debug("No country found via regex check.")

        # try if country can be found with Levenshtein
        for country in self._countrylist:
            for item in splitted:
                if (
                    lv.ratio(country[0], item) > 0.88
                    and lv.distance(country[0], item) <= 1
                ):
                    return country[0]

        log.debug("No country found via Levenshtein.")

        normalsplit = [item.strip(",; ") for item in data.split()]

        for item in normalsplit:
            if any(state == item for state in self._us_states):
                return "united states"
        log.debug("No country found by matching US states.")

        return False

    def _fix_pub_date(self, date):
        """Change invalid 00 to 01 in YYYY-MM-DD"""
        return date.replace("-00", "-01")

    def _strip_accents(self, s):
        """
        Shamelessly taken from https://stackoverflow.com/a/517974/1265357
        """
        return "".join(
            c
            for c in unicodedata.normalize("NFD", s)
            if unicodedata.category(c) != "Mn"
        )
