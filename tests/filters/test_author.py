import bajer_importer.filters.author as author
import mock

author_present = mock.Mock()
author_present.first_author = 'first author'

author_none = mock.Mock()
author_none.first_author = None

author_empty = mock.Mock()
author_empty.first_author = ''

def test_author_present():
    assert author.dofilter(author_present) == (True, None)

def test_author_none():
    assert author.dofilter(author_none) == (False, 'First author empty.')

def test_author_empty():
    assert author.dofilter(author_empty) == (False, 'First author empty.')
