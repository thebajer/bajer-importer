import bajer_importer.filters.bad_words as bad_words
import mock

row_bad_title = mock.Mock()
row_bad_title.title = ['Letters to the editor']
row_bad_title.keyword = ['radiation mechanisms: thermal', 'astrobiology']

row_bad_keyword = mock.Mock()
row_bad_keyword.title = ['This is a title']
row_bad_keyword.keyword = ['radiation mechanisms: thermal', 'astrobiology', 'with some other words Errata']


def test_title_bad():
    assert bad_words.dofilter(row_bad_title) == (False, "Found bad word --letter-- in title.")


def test_keyword_bad():
    assert bad_words.dofilter(row_bad_keyword) == (False, "Found bad word --errata-- in keywords.")
